﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class MainForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MainForm))
        Me.CsvInputTextBox = New System.Windows.Forms.TextBox()
        Me.GeneratePaymentPartReceiptCsvButton = New System.Windows.Forms.Button()
        Me.PaymentPartReceiptPictureBox = New System.Windows.Forms.PictureBox()
        Me.MainTabControl = New System.Windows.Forms.TabControl()
        Me.JsonInputTabPage = New System.Windows.Forms.TabPage()
        Me.ValidateJsonButton = New System.Windows.Forms.Button()
        Me.GenerateSpcJsonButton = New System.Windows.Forms.Button()
        Me.ScissorJsonCheckBox = New System.Windows.Forms.CheckBox()
        Me.SeparationTextJsonCheckBox = New System.Windows.Forms.CheckBox()
        Me.BoundaryLinesJsonCheckBox = New System.Windows.Forms.CheckBox()
        Me.GenerateSwissQRCodeJsonButton = New System.Windows.Forms.Button()
        Me.GeneratePaymentPartReceiptJsonButton = New System.Windows.Forms.Button()
        Me.JsonInputTextBox = New System.Windows.Forms.TextBox()
        Me.CsvInputTabPage = New System.Windows.Forms.TabPage()
        Me.ValidateCsvButton = New System.Windows.Forms.Button()
        Me.GenerateSpcCsvButton = New System.Windows.Forms.Button()
        Me.ScissorsCheckBox = New System.Windows.Forms.CheckBox()
        Me.SeparationTextCheckBox = New System.Windows.Forms.CheckBox()
        Me.BoundaryLinesCheckBox = New System.Windows.Forms.CheckBox()
        Me.GenerateSwissQRCodeCsvButton = New System.Windows.Forms.Button()
        Me.PaymentPartReceiptOutputTabPage = New System.Windows.Forms.TabPage()
        Me.SwissQrCodeTabPage = New System.Windows.Forms.TabPage()
        Me.SwissQrCodePictureBox = New System.Windows.Forms.PictureBox()
        Me.SpcTabPage = New System.Windows.Forms.TabPage()
        Me.SpcTextBox = New System.Windows.Forms.TextBox()
        Me.ValidationTabPage = New System.Windows.Forms.TabPage()
        Me.ValidateAlButton = New System.Windows.Forms.Button()
        Me.CountryCodeTextBox = New System.Windows.Forms.TextBox()
        Me.QrReferenceTextBox = New System.Windows.Forms.TextBox()
        Me.QrIbanTextBox = New System.Windows.Forms.TextBox()
        Me.IbanTextBox = New System.Windows.Forms.TextBox()
        Me.CreditorReferenceTextBox = New System.Windows.Forms.TextBox()
        Me.CountryCodeabel = New System.Windows.Forms.Label()
        Me.CreditorLabel = New System.Windows.Forms.Label()
        Me.QrReferenceLabel = New System.Windows.Forms.Label()
        Me.QrIBANLabel = New System.Windows.Forms.Label()
        Me.IbanLabel = New System.Windows.Forms.Label()
        Me.StatusStrip = New System.Windows.Forms.StatusStrip()
        Me.StatusLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me.StringTextBox = New System.Windows.Forms.TextBox()
        Me.StringLabel = New System.Windows.Forms.Label()
        CType(Me.PaymentPartReceiptPictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MainTabControl.SuspendLayout()
        Me.JsonInputTabPage.SuspendLayout()
        Me.CsvInputTabPage.SuspendLayout()
        Me.PaymentPartReceiptOutputTabPage.SuspendLayout()
        Me.SwissQrCodeTabPage.SuspendLayout()
        CType(Me.SwissQrCodePictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SpcTabPage.SuspendLayout()
        Me.ValidationTabPage.SuspendLayout()
        Me.StatusStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        'CsvInputTextBox
        '
        Me.CsvInputTextBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CsvInputTextBox.Location = New System.Drawing.Point(8, 6)
        Me.CsvInputTextBox.Multiline = True
        Me.CsvInputTextBox.Name = "CsvInputTextBox"
        Me.CsvInputTextBox.Size = New System.Drawing.Size(1101, 327)
        Me.CsvInputTextBox.TabIndex = 0
        Me.CsvInputTextBox.Text = resources.GetString("CsvInputTextBox.Text")
        '
        'GeneratePaymentPartReceiptCsvButton
        '
        Me.GeneratePaymentPartReceiptCsvButton.Location = New System.Drawing.Point(9, 340)
        Me.GeneratePaymentPartReceiptCsvButton.Name = "GeneratePaymentPartReceiptCsvButton"
        Me.GeneratePaymentPartReceiptCsvButton.Size = New System.Drawing.Size(138, 23)
        Me.GeneratePaymentPartReceiptCsvButton.TabIndex = 2
        Me.GeneratePaymentPartReceiptCsvButton.Text = "Generate Payment Part & Receipt"
        Me.GeneratePaymentPartReceiptCsvButton.UseVisualStyleBackColor = True
        '
        'PaymentPartReceiptPictureBox
        '
        Me.PaymentPartReceiptPictureBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PaymentPartReceiptPictureBox.Location = New System.Drawing.Point(3, 3)
        Me.PaymentPartReceiptPictureBox.Name = "PaymentPartReceiptPictureBox"
        Me.PaymentPartReceiptPictureBox.Size = New System.Drawing.Size(1109, 642)
        Me.PaymentPartReceiptPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PaymentPartReceiptPictureBox.TabIndex = 1
        Me.PaymentPartReceiptPictureBox.TabStop = False
        '
        'MainTabControl
        '
        Me.MainTabControl.Controls.Add(Me.JsonInputTabPage)
        Me.MainTabControl.Controls.Add(Me.CsvInputTabPage)
        Me.MainTabControl.Controls.Add(Me.PaymentPartReceiptOutputTabPage)
        Me.MainTabControl.Controls.Add(Me.SwissQrCodeTabPage)
        Me.MainTabControl.Controls.Add(Me.SpcTabPage)
        Me.MainTabControl.Controls.Add(Me.ValidationTabPage)
        Me.MainTabControl.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MainTabControl.Location = New System.Drawing.Point(0, 0)
        Me.MainTabControl.Name = "MainTabControl"
        Me.MainTabControl.SelectedIndex = 0
        Me.MainTabControl.Size = New System.Drawing.Size(1123, 674)
        Me.MainTabControl.TabIndex = 1
        '
        'JsonInputTabPage
        '
        Me.JsonInputTabPage.Controls.Add(Me.ValidateJsonButton)
        Me.JsonInputTabPage.Controls.Add(Me.GenerateSpcJsonButton)
        Me.JsonInputTabPage.Controls.Add(Me.ScissorJsonCheckBox)
        Me.JsonInputTabPage.Controls.Add(Me.SeparationTextJsonCheckBox)
        Me.JsonInputTabPage.Controls.Add(Me.BoundaryLinesJsonCheckBox)
        Me.JsonInputTabPage.Controls.Add(Me.GenerateSwissQRCodeJsonButton)
        Me.JsonInputTabPage.Controls.Add(Me.GeneratePaymentPartReceiptJsonButton)
        Me.JsonInputTabPage.Controls.Add(Me.JsonInputTextBox)
        Me.JsonInputTabPage.Location = New System.Drawing.Point(4, 22)
        Me.JsonInputTabPage.Name = "JsonInputTabPage"
        Me.JsonInputTabPage.Padding = New System.Windows.Forms.Padding(3)
        Me.JsonInputTabPage.Size = New System.Drawing.Size(1115, 648)
        Me.JsonInputTabPage.TabIndex = 3
        Me.JsonInputTabPage.Text = "JSON Input"
        Me.JsonInputTabPage.UseVisualStyleBackColor = True
        '
        'ValidateJsonButton
        '
        Me.ValidateJsonButton.Location = New System.Drawing.Point(492, 537)
        Me.ValidateJsonButton.Name = "ValidateJsonButton"
        Me.ValidateJsonButton.Size = New System.Drawing.Size(75, 23)
        Me.ValidateJsonButton.TabIndex = 11
        Me.ValidateJsonButton.Text = "Validate"
        Me.ValidateJsonButton.UseVisualStyleBackColor = True
        '
        'GenerateSpcJsonButton
        '
        Me.GenerateSpcJsonButton.Location = New System.Drawing.Point(304, 537)
        Me.GenerateSpcJsonButton.Name = "GenerateSpcJsonButton"
        Me.GenerateSpcJsonButton.Size = New System.Drawing.Size(181, 23)
        Me.GenerateSpcJsonButton.TabIndex = 10
        Me.GenerateSpcJsonButton.Text = "Generate Swiss Payments Code"
        Me.GenerateSpcJsonButton.UseVisualStyleBackColor = True
        '
        'ScissorJsonCheckBox
        '
        Me.ScissorJsonCheckBox.AutoSize = True
        Me.ScissorJsonCheckBox.Checked = True
        Me.ScissorJsonCheckBox.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ScissorJsonCheckBox.Location = New System.Drawing.Point(8, 589)
        Me.ScissorJsonCheckBox.Name = "ScissorJsonCheckBox"
        Me.ScissorJsonCheckBox.Size = New System.Drawing.Size(65, 17)
        Me.ScissorJsonCheckBox.TabIndex = 9
        Me.ScissorJsonCheckBox.Text = "Scissors"
        Me.ScissorJsonCheckBox.UseVisualStyleBackColor = True
        '
        'SeparationTextJsonCheckBox
        '
        Me.SeparationTextJsonCheckBox.AutoSize = True
        Me.SeparationTextJsonCheckBox.Checked = True
        Me.SeparationTextJsonCheckBox.CheckState = System.Windows.Forms.CheckState.Checked
        Me.SeparationTextJsonCheckBox.Location = New System.Drawing.Point(8, 612)
        Me.SeparationTextJsonCheckBox.Name = "SeparationTextJsonCheckBox"
        Me.SeparationTextJsonCheckBox.Size = New System.Drawing.Size(101, 17)
        Me.SeparationTextJsonCheckBox.TabIndex = 8
        Me.SeparationTextJsonCheckBox.Text = "Separation Text"
        Me.SeparationTextJsonCheckBox.UseVisualStyleBackColor = True
        '
        'BoundaryLinesJsonCheckBox
        '
        Me.BoundaryLinesJsonCheckBox.AutoSize = True
        Me.BoundaryLinesJsonCheckBox.Checked = True
        Me.BoundaryLinesJsonCheckBox.CheckState = System.Windows.Forms.CheckState.Checked
        Me.BoundaryLinesJsonCheckBox.Location = New System.Drawing.Point(8, 566)
        Me.BoundaryLinesJsonCheckBox.Name = "BoundaryLinesJsonCheckBox"
        Me.BoundaryLinesJsonCheckBox.Size = New System.Drawing.Size(99, 17)
        Me.BoundaryLinesJsonCheckBox.TabIndex = 7
        Me.BoundaryLinesJsonCheckBox.Text = "Boundary Lines"
        Me.BoundaryLinesJsonCheckBox.UseVisualStyleBackColor = True
        '
        'GenerateSwissQRCodeJsonButton
        '
        Me.GenerateSwissQRCodeJsonButton.Location = New System.Drawing.Point(152, 537)
        Me.GenerateSwissQRCodeJsonButton.Name = "GenerateSwissQRCodeJsonButton"
        Me.GenerateSwissQRCodeJsonButton.Size = New System.Drawing.Size(146, 23)
        Me.GenerateSwissQRCodeJsonButton.TabIndex = 4
        Me.GenerateSwissQRCodeJsonButton.Text = "Generate Swiss QR Code"
        Me.GenerateSwissQRCodeJsonButton.UseVisualStyleBackColor = True
        '
        'GeneratePaymentPartReceiptJsonButton
        '
        Me.GeneratePaymentPartReceiptJsonButton.Location = New System.Drawing.Point(8, 537)
        Me.GeneratePaymentPartReceiptJsonButton.Name = "GeneratePaymentPartReceiptJsonButton"
        Me.GeneratePaymentPartReceiptJsonButton.Size = New System.Drawing.Size(138, 23)
        Me.GeneratePaymentPartReceiptJsonButton.TabIndex = 3
        Me.GeneratePaymentPartReceiptJsonButton.Text = "Generate Payment Part & Receipt"
        Me.GeneratePaymentPartReceiptJsonButton.UseVisualStyleBackColor = True
        '
        'JsonInputTextBox
        '
        Me.JsonInputTextBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.JsonInputTextBox.Location = New System.Drawing.Point(8, 6)
        Me.JsonInputTextBox.Multiline = True
        Me.JsonInputTextBox.Name = "JsonInputTextBox"
        Me.JsonInputTextBox.Size = New System.Drawing.Size(1101, 525)
        Me.JsonInputTextBox.TabIndex = 0
        Me.JsonInputTextBox.Text = resources.GetString("JsonInputTextBox.Text")
        '
        'CsvInputTabPage
        '
        Me.CsvInputTabPage.Controls.Add(Me.ValidateCsvButton)
        Me.CsvInputTabPage.Controls.Add(Me.GenerateSpcCsvButton)
        Me.CsvInputTabPage.Controls.Add(Me.ScissorsCheckBox)
        Me.CsvInputTabPage.Controls.Add(Me.SeparationTextCheckBox)
        Me.CsvInputTabPage.Controls.Add(Me.BoundaryLinesCheckBox)
        Me.CsvInputTabPage.Controls.Add(Me.GenerateSwissQRCodeCsvButton)
        Me.CsvInputTabPage.Controls.Add(Me.CsvInputTextBox)
        Me.CsvInputTabPage.Controls.Add(Me.GeneratePaymentPartReceiptCsvButton)
        Me.CsvInputTabPage.Location = New System.Drawing.Point(4, 22)
        Me.CsvInputTabPage.Name = "CsvInputTabPage"
        Me.CsvInputTabPage.Padding = New System.Windows.Forms.Padding(3)
        Me.CsvInputTabPage.Size = New System.Drawing.Size(1115, 648)
        Me.CsvInputTabPage.TabIndex = 0
        Me.CsvInputTabPage.Text = "CSV Input"
        Me.CsvInputTabPage.UseVisualStyleBackColor = True
        '
        'ValidateCsvButton
        '
        Me.ValidateCsvButton.Location = New System.Drawing.Point(487, 340)
        Me.ValidateCsvButton.Name = "ValidateCsvButton"
        Me.ValidateCsvButton.Size = New System.Drawing.Size(75, 23)
        Me.ValidateCsvButton.TabIndex = 8
        Me.ValidateCsvButton.Text = "Validate"
        Me.ValidateCsvButton.UseVisualStyleBackColor = True
        '
        'GenerateSpcCsvButton
        '
        Me.GenerateSpcCsvButton.Location = New System.Drawing.Point(306, 340)
        Me.GenerateSpcCsvButton.Name = "GenerateSpcCsvButton"
        Me.GenerateSpcCsvButton.Size = New System.Drawing.Size(174, 23)
        Me.GenerateSpcCsvButton.TabIndex = 7
        Me.GenerateSpcCsvButton.Text = "Generate Swiss Payments Code"
        Me.GenerateSpcCsvButton.UseVisualStyleBackColor = True
        '
        'ScissorsCheckBox
        '
        Me.ScissorsCheckBox.AutoSize = True
        Me.ScissorsCheckBox.Checked = True
        Me.ScissorsCheckBox.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ScissorsCheckBox.Location = New System.Drawing.Point(9, 393)
        Me.ScissorsCheckBox.Name = "ScissorsCheckBox"
        Me.ScissorsCheckBox.Size = New System.Drawing.Size(65, 17)
        Me.ScissorsCheckBox.TabIndex = 6
        Me.ScissorsCheckBox.Text = "Scissors"
        Me.ScissorsCheckBox.UseVisualStyleBackColor = True
        '
        'SeparationTextCheckBox
        '
        Me.SeparationTextCheckBox.AutoSize = True
        Me.SeparationTextCheckBox.Checked = True
        Me.SeparationTextCheckBox.CheckState = System.Windows.Forms.CheckState.Checked
        Me.SeparationTextCheckBox.Location = New System.Drawing.Point(9, 416)
        Me.SeparationTextCheckBox.Name = "SeparationTextCheckBox"
        Me.SeparationTextCheckBox.Size = New System.Drawing.Size(101, 17)
        Me.SeparationTextCheckBox.TabIndex = 5
        Me.SeparationTextCheckBox.Text = "Separation Text"
        Me.SeparationTextCheckBox.UseVisualStyleBackColor = True
        '
        'BoundaryLinesCheckBox
        '
        Me.BoundaryLinesCheckBox.AutoSize = True
        Me.BoundaryLinesCheckBox.Checked = True
        Me.BoundaryLinesCheckBox.CheckState = System.Windows.Forms.CheckState.Checked
        Me.BoundaryLinesCheckBox.Location = New System.Drawing.Point(9, 370)
        Me.BoundaryLinesCheckBox.Name = "BoundaryLinesCheckBox"
        Me.BoundaryLinesCheckBox.Size = New System.Drawing.Size(99, 17)
        Me.BoundaryLinesCheckBox.TabIndex = 4
        Me.BoundaryLinesCheckBox.Text = "Boundary Lines"
        Me.BoundaryLinesCheckBox.UseVisualStyleBackColor = True
        '
        'GenerateSwissQRCodeCsvButton
        '
        Me.GenerateSwissQRCodeCsvButton.Location = New System.Drawing.Point(153, 340)
        Me.GenerateSwissQRCodeCsvButton.Name = "GenerateSwissQRCodeCsvButton"
        Me.GenerateSwissQRCodeCsvButton.Size = New System.Drawing.Size(146, 23)
        Me.GenerateSwissQRCodeCsvButton.TabIndex = 3
        Me.GenerateSwissQRCodeCsvButton.Text = "Generate Swiss QR Code"
        Me.GenerateSwissQRCodeCsvButton.UseVisualStyleBackColor = True
        '
        'PaymentPartReceiptOutputTabPage
        '
        Me.PaymentPartReceiptOutputTabPage.Controls.Add(Me.PaymentPartReceiptPictureBox)
        Me.PaymentPartReceiptOutputTabPage.Location = New System.Drawing.Point(4, 22)
        Me.PaymentPartReceiptOutputTabPage.Name = "PaymentPartReceiptOutputTabPage"
        Me.PaymentPartReceiptOutputTabPage.Padding = New System.Windows.Forms.Padding(3)
        Me.PaymentPartReceiptOutputTabPage.Size = New System.Drawing.Size(1115, 648)
        Me.PaymentPartReceiptOutputTabPage.TabIndex = 1
        Me.PaymentPartReceiptOutputTabPage.Text = "QR Bill Output"
        Me.PaymentPartReceiptOutputTabPage.UseVisualStyleBackColor = True
        '
        'SwissQrCodeTabPage
        '
        Me.SwissQrCodeTabPage.Controls.Add(Me.SwissQrCodePictureBox)
        Me.SwissQrCodeTabPage.Location = New System.Drawing.Point(4, 22)
        Me.SwissQrCodeTabPage.Name = "SwissQrCodeTabPage"
        Me.SwissQrCodeTabPage.Padding = New System.Windows.Forms.Padding(3)
        Me.SwissQrCodeTabPage.Size = New System.Drawing.Size(1115, 648)
        Me.SwissQrCodeTabPage.TabIndex = 2
        Me.SwissQrCodeTabPage.Text = "Swiss QR Code"
        Me.SwissQrCodeTabPage.UseVisualStyleBackColor = True
        '
        'SwissQrCodePictureBox
        '
        Me.SwissQrCodePictureBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SwissQrCodePictureBox.Location = New System.Drawing.Point(3, 3)
        Me.SwissQrCodePictureBox.Name = "SwissQrCodePictureBox"
        Me.SwissQrCodePictureBox.Size = New System.Drawing.Size(1109, 642)
        Me.SwissQrCodePictureBox.TabIndex = 0
        Me.SwissQrCodePictureBox.TabStop = False
        '
        'SpcTabPage
        '
        Me.SpcTabPage.Controls.Add(Me.SpcTextBox)
        Me.SpcTabPage.Location = New System.Drawing.Point(4, 22)
        Me.SpcTabPage.Name = "SpcTabPage"
        Me.SpcTabPage.Padding = New System.Windows.Forms.Padding(3)
        Me.SpcTabPage.Size = New System.Drawing.Size(1115, 648)
        Me.SpcTabPage.TabIndex = 4
        Me.SpcTabPage.Text = "Swiss Payments Code"
        Me.SpcTabPage.UseVisualStyleBackColor = True
        '
        'SpcTextBox
        '
        Me.SpcTextBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SpcTextBox.Location = New System.Drawing.Point(3, 3)
        Me.SpcTextBox.Multiline = True
        Me.SpcTextBox.Name = "SpcTextBox"
        Me.SpcTextBox.ReadOnly = True
        Me.SpcTextBox.Size = New System.Drawing.Size(1109, 642)
        Me.SpcTextBox.TabIndex = 0
        '
        'ValidationTabPage
        '
        Me.ValidationTabPage.Controls.Add(Me.StringLabel)
        Me.ValidationTabPage.Controls.Add(Me.StringTextBox)
        Me.ValidationTabPage.Controls.Add(Me.ValidateAlButton)
        Me.ValidationTabPage.Controls.Add(Me.CountryCodeTextBox)
        Me.ValidationTabPage.Controls.Add(Me.QrReferenceTextBox)
        Me.ValidationTabPage.Controls.Add(Me.QrIbanTextBox)
        Me.ValidationTabPage.Controls.Add(Me.IbanTextBox)
        Me.ValidationTabPage.Controls.Add(Me.CreditorReferenceTextBox)
        Me.ValidationTabPage.Controls.Add(Me.CountryCodeabel)
        Me.ValidationTabPage.Controls.Add(Me.CreditorLabel)
        Me.ValidationTabPage.Controls.Add(Me.QrReferenceLabel)
        Me.ValidationTabPage.Controls.Add(Me.QrIBANLabel)
        Me.ValidationTabPage.Controls.Add(Me.IbanLabel)
        Me.ValidationTabPage.Location = New System.Drawing.Point(4, 22)
        Me.ValidationTabPage.Name = "ValidationTabPage"
        Me.ValidationTabPage.Padding = New System.Windows.Forms.Padding(3)
        Me.ValidationTabPage.Size = New System.Drawing.Size(1115, 648)
        Me.ValidationTabPage.TabIndex = 5
        Me.ValidationTabPage.Text = "Validations"
        Me.ValidationTabPage.UseVisualStyleBackColor = True
        '
        'ValidateAlButton
        '
        Me.ValidateAlButton.Location = New System.Drawing.Point(105, 169)
        Me.ValidateAlButton.Name = "ValidateAlButton"
        Me.ValidateAlButton.Size = New System.Drawing.Size(75, 23)
        Me.ValidateAlButton.TabIndex = 7
        Me.ValidateAlButton.Text = "Validate All"
        Me.ValidateAlButton.UseVisualStyleBackColor = True
        '
        'CountryCodeTextBox
        '
        Me.CountryCodeTextBox.Location = New System.Drawing.Point(105, 143)
        Me.CountryCodeTextBox.MaxLength = 2
        Me.CountryCodeTextBox.Name = "CountryCodeTextBox"
        Me.CountryCodeTextBox.Size = New System.Drawing.Size(32, 20)
        Me.CountryCodeTextBox.TabIndex = 6
        Me.CountryCodeTextBox.Text = "CH"
        '
        'QrReferenceTextBox
        '
        Me.QrReferenceTextBox.Location = New System.Drawing.Point(105, 91)
        Me.QrReferenceTextBox.Name = "QrReferenceTextBox"
        Me.QrReferenceTextBox.Size = New System.Drawing.Size(195, 20)
        Me.QrReferenceTextBox.TabIndex = 4
        Me.QrReferenceTextBox.Text = "11 00012 34560 00000 00008 13457"
        '
        'QrIbanTextBox
        '
        Me.QrIbanTextBox.Location = New System.Drawing.Point(105, 65)
        Me.QrIbanTextBox.Name = "QrIbanTextBox"
        Me.QrIbanTextBox.Size = New System.Drawing.Size(195, 20)
        Me.QrIbanTextBox.TabIndex = 3
        Me.QrIbanTextBox.Text = "CH44 3199 9123 0008 8901 2"
        '
        'IbanTextBox
        '
        Me.IbanTextBox.Location = New System.Drawing.Point(105, 37)
        Me.IbanTextBox.Name = "IbanTextBox"
        Me.IbanTextBox.Size = New System.Drawing.Size(195, 20)
        Me.IbanTextBox.TabIndex = 2
        Me.IbanTextBox.Text = "CH39 0870 4016 0754 7300 7"
        '
        'CreditorReferenceTextBox
        '
        Me.CreditorReferenceTextBox.Location = New System.Drawing.Point(105, 117)
        Me.CreditorReferenceTextBox.Name = "CreditorReferenceTextBox"
        Me.CreditorReferenceTextBox.Size = New System.Drawing.Size(195, 20)
        Me.CreditorReferenceTextBox.TabIndex = 5
        Me.CreditorReferenceTextBox.Text = "RF45 1234 5123 45"
        '
        'CountryCodeabel
        '
        Me.CountryCodeabel.AutoSize = True
        Me.CountryCodeabel.Location = New System.Drawing.Point(3, 146)
        Me.CountryCodeabel.Name = "CountryCodeabel"
        Me.CountryCodeabel.Size = New System.Drawing.Size(71, 13)
        Me.CountryCodeabel.TabIndex = 4
        Me.CountryCodeabel.Text = "Country Code"
        '
        'CreditorLabel
        '
        Me.CreditorLabel.AutoSize = True
        Me.CreditorLabel.Location = New System.Drawing.Point(3, 120)
        Me.CreditorLabel.Name = "CreditorLabel"
        Me.CreditorLabel.Size = New System.Drawing.Size(96, 13)
        Me.CreditorLabel.TabIndex = 3
        Me.CreditorLabel.Text = "Creditor Reference"
        '
        'QrReferenceLabel
        '
        Me.QrReferenceLabel.AutoSize = True
        Me.QrReferenceLabel.Location = New System.Drawing.Point(3, 94)
        Me.QrReferenceLabel.Name = "QrReferenceLabel"
        Me.QrReferenceLabel.Size = New System.Drawing.Size(76, 13)
        Me.QrReferenceLabel.TabIndex = 2
        Me.QrReferenceLabel.Text = "QR Reference"
        '
        'QrIBANLabel
        '
        Me.QrIBANLabel.AutoSize = True
        Me.QrIBANLabel.Location = New System.Drawing.Point(3, 68)
        Me.QrIBANLabel.Name = "QrIBANLabel"
        Me.QrIBANLabel.Size = New System.Drawing.Size(51, 13)
        Me.QrIBANLabel.TabIndex = 1
        Me.QrIBANLabel.Text = "QR IBAN"
        '
        'IbanLabel
        '
        Me.IbanLabel.AutoSize = True
        Me.IbanLabel.Location = New System.Drawing.Point(3, 40)
        Me.IbanLabel.Name = "IbanLabel"
        Me.IbanLabel.Size = New System.Drawing.Size(32, 13)
        Me.IbanLabel.TabIndex = 0
        Me.IbanLabel.Text = "IBAN"
        '
        'StatusStrip
        '
        Me.StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.StatusLabel})
        Me.StatusStrip.Location = New System.Drawing.Point(0, 652)
        Me.StatusStrip.Name = "StatusStrip"
        Me.StatusStrip.Size = New System.Drawing.Size(1123, 22)
        Me.StatusStrip.TabIndex = 2
        Me.StatusStrip.Text = "StatusStrip1"
        '
        'StatusLabel
        '
        Me.StatusLabel.Name = "StatusLabel"
        Me.StatusLabel.Size = New System.Drawing.Size(0, 17)
        '
        'StringTextBox
        '
        Me.StringTextBox.Location = New System.Drawing.Point(105, 11)
        Me.StringTextBox.Name = "StringTextBox"
        Me.StringTextBox.Size = New System.Drawing.Size(195, 20)
        Me.StringTextBox.TabIndex = 1
        Me.StringTextBox.Text = "Robert Schneider AG"
        '
        'StringLabel
        '
        Me.StringLabel.AutoSize = True
        Me.StringLabel.Location = New System.Drawing.Point(3, 14)
        Me.StringLabel.Name = "StringLabel"
        Me.StringLabel.Size = New System.Drawing.Size(34, 13)
        Me.StringLabel.TabIndex = 11
        Me.StringLabel.Text = "String"
        '
        'MainForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1123, 674)
        Me.Controls.Add(Me.StatusStrip)
        Me.Controls.Add(Me.MainTabControl)
        Me.Name = "MainForm"
        Me.Text = "QR Invoice VB.NET Demo"
        CType(Me.PaymentPartReceiptPictureBox, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MainTabControl.ResumeLayout(False)
        Me.JsonInputTabPage.ResumeLayout(False)
        Me.JsonInputTabPage.PerformLayout()
        Me.CsvInputTabPage.ResumeLayout(False)
        Me.CsvInputTabPage.PerformLayout()
        Me.PaymentPartReceiptOutputTabPage.ResumeLayout(False)
        Me.SwissQrCodeTabPage.ResumeLayout(False)
        CType(Me.SwissQrCodePictureBox, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SpcTabPage.ResumeLayout(False)
        Me.SpcTabPage.PerformLayout()
        Me.ValidationTabPage.ResumeLayout(False)
        Me.ValidationTabPage.PerformLayout()
        Me.StatusStrip.ResumeLayout(False)
        Me.StatusStrip.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CsvInputTextBox As TextBox
    Friend WithEvents PaymentPartReceiptPictureBox As PictureBox
    Friend WithEvents GeneratePaymentPartReceiptCsvButton As Button
    Friend WithEvents MainTabControl As TabControl
    Friend WithEvents CsvInputTabPage As TabPage
    Friend WithEvents PaymentPartReceiptOutputTabPage As TabPage
    Friend WithEvents StatusStrip As StatusStrip
    Friend WithEvents StatusLabel As ToolStripStatusLabel
    Friend WithEvents GenerateSwissQRCodeCsvButton As Button
    Friend WithEvents SwissQrCodeTabPage As TabPage
    Friend WithEvents SwissQrCodePictureBox As PictureBox
    Friend WithEvents SeparationTextCheckBox As CheckBox
    Friend WithEvents BoundaryLinesCheckBox As CheckBox
    Friend WithEvents ScissorsCheckBox As CheckBox
    Friend WithEvents JsonInputTabPage As TabPage
    Friend WithEvents JsonInputTextBox As TextBox
    Friend WithEvents GenerateSwissQRCodeJsonButton As Button
    Friend WithEvents GeneratePaymentPartReceiptJsonButton As Button
    Friend WithEvents ScissorJsonCheckBox As CheckBox
    Friend WithEvents SeparationTextJsonCheckBox As CheckBox
    Friend WithEvents BoundaryLinesJsonCheckBox As CheckBox
    Friend WithEvents GenerateSpcJsonButton As Button
    Friend WithEvents GenerateSpcCsvButton As Button
    Friend WithEvents SpcTabPage As TabPage
    Friend WithEvents SpcTextBox As TextBox
    Friend WithEvents ValidateJsonButton As Button
    Friend WithEvents ValidateCsvButton As Button
    Friend WithEvents ValidationTabPage As TabPage
    Friend WithEvents CountryCodeabel As Label
    Friend WithEvents CreditorLabel As Label
    Friend WithEvents QrReferenceLabel As Label
    Friend WithEvents QrIBANLabel As Label
    Friend WithEvents IbanLabel As Label
    Friend WithEvents ValidateAlButton As Button
    Friend WithEvents CountryCodeTextBox As TextBox
    Friend WithEvents QrReferenceTextBox As TextBox
    Friend WithEvents QrIbanTextBox As TextBox
    Friend WithEvents IbanTextBox As TextBox
    Friend WithEvents CreditorReferenceTextBox As TextBox
    Friend WithEvents StringTextBox As TextBox
    Friend WithEvents StringLabel As Label
End Class
