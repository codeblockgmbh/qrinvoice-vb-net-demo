﻿Imports System.Text

Public Class MainForm

    Private Sub HandleBase64ErrorMessage(result As String)
        Dim message As String
        Dim data() As Byte
        data = System.Convert.FromBase64String(result)
        message = System.Text.Encoding.UTF8.GetString(data)

        MsgBox(message)
        StatusLabel.Text = ""
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles GeneratePaymentPartReceiptCsvButton.Click
        Dim qrinvoice As New QrInvoice

        StatusLabel.Text = ""

        Dim result As String = ""
        Dim watch As Stopwatch = Stopwatch.StartNew()
        Dim retCode As Integer = qrinvoice.CreatePaymentPartReceiptCsv(BoundaryLinesCheckBox.Checked, ScissorsCheckBox.Checked, SeparationTextCheckBox.Checked, System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(CsvInputTextBox.Text)), result)
        watch.Stop()
        If retCode = 0 Then
            Dim data() As Byte
            data = System.Convert.FromBase64String(result)

            Dim ms As New IO.MemoryStream(CType(data, Byte())) 'This is correct...
            Dim returnImage As Image = Image.FromStream(ms)

            PaymentPartReceiptPictureBox.Image = returnImage

            StatusLabel.Text = "Payment Part & Receipt successfully generated - took: " & watch.ElapsedMilliseconds & " ms"
            MainTabControl.SelectedTab = PaymentPartReceiptOutputTabPage
        Else
            HandleBase64ErrorMessage(result)
        End If
    End Sub

    Private Sub GenerateSwissQRCodeButton_Click(sender As Object, e As EventArgs) Handles GenerateSwissQRCodeCsvButton.Click
        Dim qrinvoice As New QrInvoice

        StatusLabel.Text = ""

        Dim result As String = ""
        Dim watch As Stopwatch = Stopwatch.StartNew()

        Dim retCode As Integer = qrinvoice.CreateQrCodeCsv(System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(CsvInputTextBox.Text)), result)
        watch.Stop()
        If retCode = 0 Then
            Dim data() As Byte
            data = System.Convert.FromBase64String(result)

            Dim ms As New IO.MemoryStream(CType(data, Byte())) 'This is correct...
            Dim returnImage As Image = Image.FromStream(ms)

            SwissQrCodePictureBox.Image = returnImage

            StatusLabel.Text = "Swiss QR Code successfully generated - took: " & watch.ElapsedMilliseconds & " ms"
            MainTabControl.SelectedTab = SwissQrCodeTabPage
        Else
            HandleBase64ErrorMessage(result)
        End If
    End Sub

    Private Sub GeneratePaymentPartReceiptJsonButton_Click(sender As Object, e As EventArgs) Handles GeneratePaymentPartReceiptJsonButton.Click
        Dim qrinvoice As New QrInvoice

        StatusLabel.Text = ""

        Dim result As String = ""
        Dim watch As Stopwatch = Stopwatch.StartNew()
        Dim retCode As Integer = qrinvoice.CreatePaymentPartReceiptJson(BoundaryLinesJsonCheckBox.Checked, ScissorJsonCheckBox.Checked, SeparationTextJsonCheckBox.Checked, System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(JsonInputTextBox.Text)), result)
        watch.Stop()
        If retCode = 0 Then
            Dim data() As Byte
            data = System.Convert.FromBase64String(result)

            Dim ms As New IO.MemoryStream(CType(data, Byte())) 'This is correct...
            Dim returnImage As Image = Image.FromStream(ms)

            PaymentPartReceiptPictureBox.Image = returnImage

            StatusLabel.Text = "Payment Part & Receipt successfully generated - took: " & watch.ElapsedMilliseconds & " ms"
            MainTabControl.SelectedTab = PaymentPartReceiptOutputTabPage
        Else
            HandleBase64ErrorMessage(result)
        End If
    End Sub

    Private Sub GenerateSwissQRCodeJsonButton_Click(sender As Object, e As EventArgs) Handles GenerateSwissQRCodeJsonButton.Click
        Dim qrinvoice As New QrInvoice

        StatusLabel.Text = ""

        Dim result As String = ""
        Dim watch As Stopwatch = Stopwatch.StartNew()

        Dim retCode As Integer = qrinvoice.CreateQrCodeJson(System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(JsonInputTextBox.Text)), result)
        watch.Stop()
        If retCode = 0 Then
            Dim data() As Byte
            data = System.Convert.FromBase64String(result)

            Dim ms As New IO.MemoryStream(CType(data, Byte())) 'This is correct...
            Dim returnImage As Image = Image.FromStream(ms)

            SwissQrCodePictureBox.Image = returnImage

            StatusLabel.Text = "Swiss QR Code successfully generated - took: " & watch.ElapsedMilliseconds & " ms"
            MainTabControl.SelectedTab = SwissQrCodeTabPage
        Else
            HandleBase64ErrorMessage(result)
        End If
    End Sub


    Private Sub GenerateSpcJsonButton_Click(sender As Object, e As EventArgs) Handles GenerateSpcJsonButton.Click
        Dim qrinvoice As New QrInvoice

        StatusLabel.Text = ""

        Dim result As String = ""
        Dim watch As Stopwatch = Stopwatch.StartNew()

        Dim retCode As Integer = qrinvoice.CreateSpcJson(System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(JsonInputTextBox.Text)), result)
        watch.Stop()
        If retCode = 0 Then
            Dim spc As String
            Dim data() As Byte
            data = System.Convert.FromBase64String(result)
            spc = System.Text.Encoding.UTF8.GetString(data)
            SpcTextBox.Text = spc.Replace(vbLf, vbNewLine)

            StatusLabel.Text = "Swiss Payments Code successfully generated - took: " & watch.ElapsedMilliseconds & " ms"
            MainTabControl.SelectedTab = SpcTabPage
        Else
            HandleBase64ErrorMessage(result)
        End If
    End Sub

    Private Sub GenerateSpcCsvButton_Click(sender As Object, e As EventArgs) Handles GenerateSpcCsvButton.Click
        Dim qrinvoice As New QrInvoice

        StatusLabel.Text = ""

        Dim result As String = ""
        Dim watch As Stopwatch = Stopwatch.StartNew()

        Dim retCode As Integer = qrinvoice.CreateSpcCsv(System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(CsvInputTextBox.Text)), result)
        watch.Stop()
        If retCode = 0 Then
            Dim spc As String
            Dim data() As Byte
            data = System.Convert.FromBase64String(result)
            spc = System.Text.Encoding.UTF8.GetString(data)

            SpcTextBox.Text = spc.Replace(vbLf, vbNewLine)

            StatusLabel.Text = "Swiss Payments Code successfully generated - took: " & watch.ElapsedMilliseconds & " ms"
            MainTabControl.SelectedTab = SpcTabPage
        Else
            HandleBase64ErrorMessage(result)
        End If
    End Sub

    Private Sub ValidateJsonButton_Click(sender As Object, e As EventArgs) Handles ValidateJsonButton.Click
        Dim qrinvoice As New QrInvoice

        StatusLabel.Text = ""

        Dim result As String = ""
        Dim watch As Stopwatch = Stopwatch.StartNew()

        Dim retCode As Integer = qrinvoice.ValidateJson(System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(JsonInputTextBox.Text)), result)
        watch.Stop()
        If retCode = 0 Then
            MsgBox("Valid")
        Else
            HandleBase64ErrorMessage(result)
        End If
    End Sub

    Private Sub ValidateCsvButton_Click(sender As Object, e As EventArgs) Handles ValidateCsvButton.Click
        Dim qrinvoice As New QrInvoice

        StatusLabel.Text = ""

        Dim result As String = ""
        Dim watch As Stopwatch = Stopwatch.StartNew()

        Dim retCode As Integer = qrinvoice.ValidateCsv(System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(CsvInputTextBox.Text)), result)
        watch.Stop()
        If retCode = 0 Then
            MsgBox("Valid")
        Else
            HandleBase64ErrorMessage(result)
        End If
    End Sub

    Private Sub ValidateAlButton_Click(sender As Object, e As EventArgs) Handles ValidateAlButton.Click
        Dim allValid As Boolean = True

        If Not QrInvoice.ValidateStringBase64(System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(StringTextBox.Text))) Then
            MsgBox("String invalid")
            allValid = False
        End If

        If Not QrInvoice.ValidateIBAN(IbanTextBox.Text, True) Then
            MsgBox("IBAN invalid")
            allValid = False
        End If

        If Not QrInvoice.ValidateQrIBAN(QrIbanTextBox.Text) Then
            MsgBox("QR-IBAN invalid")
            allValid = False
        End If

        If Not QrInvoice.ValidateQrReference(QrReferenceTextBox.Text) Then
            MsgBox("QR Reference invalid")
            allValid = False
        End If

        If Not QrInvoice.ValidateCreditorReference(CreditorReferenceTextBox.Text) Then
            MsgBox("Creditor Reference invalid")
            allValid = False
        End If

        ' TODO something wrong with country code validation
        If Not QrInvoice.ValidateCountryCode(CountryCodeTextBox.Text) Then
            MsgBox("Country Code invalid")
            allValid = False
        End If

        If allValid Then
            MsgBox("All fields in this tab contain valid values")
        End If
    End Sub

    ' Dim fs As New IO.FileStream("test.png", IO.FileMode.Create)
    ' Using bw As New IO.BinaryWriter(fs)
    '     bw.Write(data)
    '     bw.Close()
    ' End Uszing
    ' fs.Close()
End Class
