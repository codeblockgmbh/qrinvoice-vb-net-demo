﻿Imports System.Text
Imports Microsoft.VisualBasic

Imports System.Runtime.InteropServices

Public Class QrInvoice
    <DllImportAttribute("C:\\dev\\git\\qrinvoice-cpp-simple\\build\\Release\\qrinvoice_simple.dll", EntryPoint:="CreateSwissQrCodePngCsvBase64",
    SetLastError:=True, CharSet:=CharSet.Ansi, ExactSpelling:=True, CallingConvention:=CallingConvention.Cdecl)>
    Public Shared Function CreateQrCodePngCsvBase64(ByVal DesiredQrCodeSize As Integer, ByVal CsvInput As String,
                                        ByVal OutputBuffer As StringBuilder, ByRef OutputBufferLength As IntPtr,
                                        ByVal MessageBuffer As StringBuilder, ByRef MessageBufferLength As IntPtr) As Integer
        ' Leave this function empty. The DLLImport attribute forces calls to be forwarded to DLL.
    End Function

    <DllImportAttribute("C:\\dev\\git\\qrinvoice-cpp-simple\\build\\Release\\qrinvoice_simple.dll", EntryPoint:="CreateSwissQrCodePngJsonBase64",
    SetLastError:=True, CharSet:=CharSet.Ansi, ExactSpelling:=True, CallingConvention:=CallingConvention.Cdecl)>
    Public Shared Function CreateQrCodePngJsonBase64(ByVal DesiredQrCodeSize As Integer, ByVal CsvInput As String,
                                        ByVal OutputBuffer As StringBuilder, ByRef OutputBufferLength As IntPtr,
                                        ByVal MessageBuffer As StringBuilder, ByRef MessageBufferLength As IntPtr) As Integer
        ' Leave this function empty. The DLLImport attribute forces calls to be forwarded to DLL.
    End Function

    '<DllImportAttribute("C:\\dev\\git\\qrinvoice-cpp-simple\\build\\Release\\qrinvoice_simple.dll", EntryPoint:="CreatePprPngCsv",
    'SetLastError:=True, CharSet:=CharSet.Ansi, ExactSpelling:=True, CallingConvention:=CallingConvention.Cdecl)>
    'Public Shared Function CreatePprPngCsv(ByVal Language As String, ByVal PageSize As String,
    '                                            ByVal BoundaryLines As Boolean, ByVal BoundaryLineScissors As Boolean, ByVal BoundaryLineSeparationText As Boolean,
    '                                            ByVal CsvInput As String,
    '                                            ByVal OutputBuffer As Byte(), ByRef OutputBufferLength As IntPtr,
    '                                            ByVal MessageBuffer As StringBuilder, ByRef MessageBufferLength As IntPtr) As Integer
    '    ' Leave this function empty. The DLLImport attribute forces calls to be forwarded to DLL.
    'End Function

    <DllImportAttribute("C:\\dev\\git\\qrinvoice-cpp-simple\\build\\Release\\qrinvoice_simple.dll", EntryPoint:="CreatePprPngCsvBase64",
    SetLastError:=True, CharSet:=CharSet.Ansi, ExactSpelling:=True, CallingConvention:=CallingConvention.Cdecl)>
    Public Shared Function CreatePprPngCsvBase64(ByVal PageSize As String, ByVal FontFamily As String, ByVal Language As String, ByVal Resolution As Integer,
                                                ByVal BoundaryLines As Boolean, ByVal BoundaryLineScissors As Boolean, ByVal BoundaryLineSeparationText As Boolean,
                                                ByVal CsvInput As String,
                                                ByVal OutputBuffer As StringBuilder, ByRef OutputBufferLength As IntPtr,
                                                ByVal MessageBuffer As StringBuilder, ByRef MessageBufferLength As IntPtr) As Integer
        ' Leave this function empty. The DLLImport attribute forces calls to be forwarded to DLL.
    End Function


    <DllImportAttribute("C:\\dev\\git\\qrinvoice-cpp-simple\\build\\Release\\qrinvoice_simple.dll", EntryPoint:="CreatePprPngJsonBase64",
    SetLastError:=True, CharSet:=CharSet.Ansi, ExactSpelling:=True, CallingConvention:=CallingConvention.Cdecl)>
    Public Shared Function CreatePprPngJsonBase64(ByVal PageSize As String, ByVal FontFamily As String, ByVal Language As String, ByVal Resolution As Integer,
                                                ByVal BoundaryLines As Boolean, ByVal BoundaryLineScissors As Boolean, ByVal BoundaryLineSeparationText As Boolean,
                                                ByVal JsonInput As String,
                                                ByVal OutputBuffer As StringBuilder, ByRef OutputBufferLength As IntPtr,
                                                ByVal MessageBuffer As StringBuilder, ByRef MessageBufferLength As IntPtr) As Integer
        ' Leave this function empty. The DLLImport attribute forces calls to be forwarded to DLL.
    End Function

    <DllImportAttribute("C:\\dev\\git\\qrinvoice-cpp-simple\\build\\Release\\qrinvoice_simple.dll", EntryPoint:="CreateSpcCsvBase64",
    SetLastError:=True, CharSet:=CharSet.Ansi, ExactSpelling:=True, CallingConvention:=CallingConvention.Cdecl)>
    Public Shared Function CreateSpcCsvBase64(ByVal CsvInput As String,
                                                ByVal OutputBuffer As StringBuilder, ByRef OutputBufferLength As IntPtr,
                                                ByVal MessageBuffer As StringBuilder, ByRef MessageBufferLength As IntPtr) As Integer
        ' Leave this function empty. The DLLImport attribute forces calls to be forwarded to DLL.
    End Function

    <DllImportAttribute("C:\\dev\\git\\qrinvoice-cpp-simple\\build\\Release\\qrinvoice_simple.dll", EntryPoint:="CreateSpcJsonBase64",
    SetLastError:=True, CharSet:=CharSet.Ansi, ExactSpelling:=True, CallingConvention:=CallingConvention.Cdecl)>
    Public Shared Function CreateSpcJsonBase64(ByVal JsonInput As String,
                                                ByVal OutputBuffer As StringBuilder, ByRef OutputBufferLength As IntPtr,
                                                ByVal MessageBuffer As StringBuilder, ByRef MessageBufferLength As IntPtr) As Integer
        ' Leave this function empty. The DLLImport attribute forces calls to be forwarded to DLL.
    End Function

    <DllImportAttribute("C:\\dev\\git\\qrinvoice-cpp-simple\\build\\Release\\qrinvoice_simple.dll", EntryPoint:="ValidateJsonBase64",
    SetLastError:=True, CharSet:=CharSet.Ansi, ExactSpelling:=True, CallingConvention:=CallingConvention.Cdecl)>
    Public Shared Function ValidateJsonBase64(ByVal JsonInput As String,
                                                ByVal MessageBuffer As StringBuilder, ByRef MessageBufferLength As IntPtr) As Integer
        ' Leave this function empty. The DLLImport attribute forces calls to be forwarded to DLL.
    End Function

    <DllImportAttribute("C:\\dev\\git\\qrinvoice-cpp-simple\\build\\Release\\qrinvoice_simple.dll", EntryPoint:="ValidateCsvBase64",
    SetLastError:=True, CharSet:=CharSet.Ansi, ExactSpelling:=True, CallingConvention:=CallingConvention.Cdecl)>
    Public Shared Function ValidateCsvBase64(ByVal CsvInput As String,
                                                ByVal MessageBuffer As StringBuilder, ByRef MessageBufferLength As IntPtr) As Integer
        ' Leave this function empty. The DLLImport attribute forces calls to be forwarded to DLL.
    End Function


    <DllImportAttribute("C:\\dev\\git\\qrinvoice-cpp-simple\\build\\Release\\qrinvoice_simple.dll", EntryPoint:="ValidateStringBase64",
    SetLastError:=True, CharSet:=CharSet.Ansi, ExactSpelling:=True, CallingConvention:=CallingConvention.Cdecl)>
    Public Shared Function ValidateStringBase64(ByVal Str As String) As Boolean
        ' Leave this function empty. The DLLImport attribute forces calls to be forwarded to DLL.
    End Function

    <DllImportAttribute("C:\\dev\\git\\qrinvoice-cpp-simple\\build\\Release\\qrinvoice_simple.dll", EntryPoint:="ValidateIBAN",
    SetLastError:=True, CharSet:=CharSet.Ansi, ExactSpelling:=True, CallingConvention:=CallingConvention.Cdecl)>
    Public Shared Function ValidateIBAN(ByVal IBAN As String, ByVal ValidateCountryCode As Boolean) As Boolean
        ' Leave this function empty. The DLLImport attribute forces calls to be forwarded to DLL.
    End Function

    <DllImportAttribute("C:\\dev\\git\\qrinvoice-cpp-simple\\build\\Release\\qrinvoice_simple.dll", EntryPoint:="ValidateQrIBAN",
    SetLastError:=True, CharSet:=CharSet.Ansi, ExactSpelling:=True, CallingConvention:=CallingConvention.Cdecl)>
    Public Shared Function ValidateQrIBAN(ByVal IBAN As String) As Boolean
        ' Leave this function empty. The DLLImport attribute forces calls to be forwarded to DLL.
    End Function

    <DllImportAttribute("C:\\dev\\git\\qrinvoice-cpp-simple\\build\\Release\\qrinvoice_simple.dll", EntryPoint:="ValidateQrReference",
    SetLastError:=True, CharSet:=CharSet.Ansi, ExactSpelling:=True, CallingConvention:=CallingConvention.Cdecl)>
    Public Shared Function ValidateQrReference(ByVal QrReference As String) As Boolean
        ' Leave this function empty. The DLLImport attribute forces calls to be forwarded to DLL.
    End Function

    <DllImportAttribute("C:\\dev\\git\\qrinvoice-cpp-simple\\build\\Release\\qrinvoice_simple.dll", EntryPoint:="ValidateCreditorReference",
    SetLastError:=True, CharSet:=CharSet.Ansi, ExactSpelling:=True, CallingConvention:=CallingConvention.Cdecl)>
    Public Shared Function ValidateCreditorReference(ByVal CreditorReference As String) As Boolean
        ' Leave this function empty. The DLLImport attribute forces calls to be forwarded to DLL.
    End Function

    <DllImportAttribute("C:\\dev\\git\\qrinvoice-cpp-simple\\build\\Release\\qrinvoice_simple.dll", EntryPoint:="ValidateCountryCode",
    SetLastError:=True, CharSet:=CharSet.Ansi, ExactSpelling:=True, CallingConvention:=CallingConvention.Cdecl)>
    Public Shared Function ValidateCountryCode(ByVal CountryCode As String) As Boolean
        ' Leave this function empty. The DLLImport attribute forces calls to be forwarded to DLL.
    End Function

    Function CreateQrCodeCsv(ByVal CsvInput As String, ByRef Results As String) As Integer
        Dim OutputBuffer As StringBuilder = New StringBuilder(300000)
        Dim OutputBufferLength As Integer = OutputBuffer.Capacity
        Dim MessageBuffer As StringBuilder = New StringBuilder(1000)
        Dim MessageBufferLength As Integer = MessageBuffer.Capacity
        Dim retCode As Integer = CreateQrCodePngCsvBase64(500, CsvInput, OutputBuffer, OutputBufferLength, MessageBuffer, MessageBufferLength)

        If retCode = 0 Then
            Results = OutputBuffer.ToString()
        Else
            Results = MessageBuffer.ToString()
        End If

        'Debug.Assert(Results.Length = OutputBufferLength)
        Return retCode
    End Function


    Function CreateQrCodeJson(ByVal JsonInput As String, ByRef Results As String) As Integer
        Dim OutputBuffer As StringBuilder = New StringBuilder(300000)
        Dim OutputBufferLength As Integer = OutputBuffer.Capacity
        Dim MessageBuffer As StringBuilder = New StringBuilder(1000)
        Dim MessageBufferLength As Integer = MessageBuffer.Capacity
        Dim retCode As Integer = CreateQrCodePngJsonBase64(500, JsonInput, OutputBuffer, OutputBufferLength, MessageBuffer, MessageBufferLength)

        If retCode = 0 Then
            Results = OutputBuffer.ToString()
        Else
            Results = MessageBuffer.ToString()
        End If

        'Debug.Assert(Results.Length = OutputBufferLength)
        Return retCode
    End Function


    Function CreatePaymentPartReceiptJson(ByVal BoundaryLines As Boolean,
                                       ByVal BoundaryLineScissors As Boolean,
                                       ByVal BoundaryLineSeparationText As Boolean,
                                       ByVal JsonInput As String, ByRef Results As String) As Integer
        Dim OutputBuffer As StringBuilder = New StringBuilder(300000)
        Dim OutputBufferLength As Integer = OutputBuffer.Capacity
        Dim MessageBuffer As StringBuilder = New StringBuilder(1000)
        Dim MessageBufferLength As Integer = MessageBuffer.Capacity

        Dim retCode As Integer = CreatePprPngJsonBase64("DIN_LANG", "LiberationSans", "GERMAN", 150, BoundaryLines, BoundaryLineScissors, BoundaryLineSeparationText, JsonInput, OutputBuffer, OutputBufferLength, MessageBuffer, MessageBufferLength)

        If retCode = 0 Then
            Results = OutputBuffer.ToString()
        Else
            Results = MessageBuffer.ToString()
        End If

        'Debug.Assert(Results.Length = OutputBufferLength)
        Return retCode
    End Function


    Function CreatePaymentPartReceiptCsv(ByVal BoundaryLines As Boolean,
                                       ByVal BoundaryLineScissors As Boolean,
                                       ByVal BoundaryLineSeparationText As Boolean,
                                       ByVal CsvInput As String, ByRef Results As String) As Integer
        Dim OutputBuffer As StringBuilder = New StringBuilder(300000)
        Dim OutputBufferLength As Integer = OutputBuffer.Capacity
        Dim MessageBuffer As StringBuilder = New StringBuilder(1000)
        Dim MessageBufferLength As Integer = MessageBuffer.Capacity

        Dim retCode As Integer = CreatePprPngCsvBase64("DIN_LANG", "LiberationSans", "GERMAN", 150, BoundaryLines, BoundaryLineScissors, BoundaryLineSeparationText, CsvInput, OutputBuffer, OutputBufferLength, MessageBuffer, MessageBufferLength)

        If retCode = 0 Then
            Results = OutputBuffer.ToString()
        Else
            Results = MessageBuffer.ToString()
        End If

        'Debug.Assert(Results.Length = OutputBufferLength)
        Return retCode
    End Function


    ' Function CreatePaymentPartReceiptRaw(ByVal BoundaryLines As Boolean,
    '                                      ByVal BoundaryLineScissors As Boolean,
    '                                      ByVal BoundaryLineSeparationText As Boolean,
    '                                      ByVal CsvInput As String, ByRef Results As String) As Integer
    '     Dim OutputBuffer(1900000) As Byte
    '     Dim OutputBufferLength As Integer = OutputBuffer.Length
    '     Dim MessageBuffer As StringBuilder = New StringBuilder(1000)
    '     Dim MessageBufferLength As Integer = MessageBuffer.Capacity
    '
    '     Dim retCode As Integer = CreatePprPngCsv("GERMAN", "DIN_LANG", BoundaryLines, BoundaryLineScissors, BoundaryLineSeparationText, CsvInput, OutputBuffer, OutputBufferLength, MessageBuffer, MessageBufferLength)
    '
    '     If retCode = 0 Then
    '         'Results = OutputBuffer.ToString()
    '     Else
    '         'Results = MessageBuffer.ToString()
    '     End If
    '
    '     'Debug.Assert(Results.Length = OutputBufferLength)
    '     Return retCode
    ' End Function


    Function CreateSpcCsv(ByVal JsonInput As String, ByRef Results As String) As Integer
        Dim OutputBuffer As StringBuilder = New StringBuilder(300000)
        Dim OutputBufferLength As Integer = OutputBuffer.Capacity
        Dim MessageBuffer As StringBuilder = New StringBuilder(1000)
        Dim MessageBufferLength As Integer = MessageBuffer.Capacity

        Dim retCode As Integer = CreateSpcCsvBase64(JsonInput, OutputBuffer, OutputBufferLength, MessageBuffer, MessageBufferLength)

        If retCode = 0 Then
            Results = OutputBuffer.ToString()
        Else
            Results = MessageBuffer.ToString()
        End If

        'Debug.Assert(Results.Length = OutputBufferLength)
        Return retCode
    End Function

    Function CreateSpcJson(ByVal JsonInput As String, ByRef Results As String) As Integer
        Dim OutputBuffer As StringBuilder = New StringBuilder(300000)
        Dim OutputBufferLength As Integer = OutputBuffer.Capacity
        Dim MessageBuffer As StringBuilder = New StringBuilder(1000)
        Dim MessageBufferLength As Integer = MessageBuffer.Capacity

        Dim retCode As Integer = CreateSpcJsonBase64(JsonInput, OutputBuffer, OutputBufferLength, MessageBuffer, MessageBufferLength)

        If retCode = 0 Then
            Results = OutputBuffer.ToString()
        Else
            Results = MessageBuffer.ToString()
        End If

        'Debug.Assert(Results.Length = OutputBufferLength)
        Return retCode
    End Function

    Function ValidateCsv(ByVal CsvInput As String, ByRef Results As String) As Integer
        Dim MessageBuffer As StringBuilder = New StringBuilder(1000)
        Dim MessageBufferLength As Integer = MessageBuffer.Capacity

        Dim retCode As Integer = ValidateCsvBase64(CsvInput, MessageBuffer, MessageBufferLength)

        Results = MessageBuffer.ToString()

        Return retCode
    End Function

    Function ValidateJson(ByVal JsonInput As String, ByRef Results As String) As Integer
        Dim MessageBuffer As StringBuilder = New StringBuilder(1000)
        Dim MessageBufferLength As Integer = MessageBuffer.Capacity

        Dim retCode As Integer = ValidateJsonBase64(JsonInput, MessageBuffer, MessageBufferLength)

        Results = MessageBuffer.ToString()

        Return retCode
    End Function

End Class
